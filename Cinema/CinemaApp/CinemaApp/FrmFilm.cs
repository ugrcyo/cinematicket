﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBaseLayer;
using ServiceLayer.Common.DTOS;
using ServiceLayer.Manage;

namespace CinemaApp
{
    public partial class FrmFilm : Form
    {
        public FrmFilm()
        {
            InitializeComponent();
        }

        SaloonManage salonManage = new SaloonManage();
       

        private void comboBoxSaloon_Enter(object sender, EventArgs e)
        {
            
        }

       

        private void comboBoxSaloon_Click(object sender, EventArgs e)
        {
           
        }

        private void comboBoxFilmGenre_Enter(object sender, EventArgs e)
        {
            

        }


        
        private void buttonAddSession_Click(object sender, EventArgs e)
        {
            
           
        }

        

        private void FrmFilm_Load(object sender, EventArgs e)
        {
            GenerateDataGrid();
            FillSaloonList();
            FillGenreList();

            comboBoxFilmList.DataSource = filmManage.Lists();
            comboBoxFilmList.ValueMember = "ID";
            comboBoxFilmList.DisplayMember = "Name";
            

        }
        FilmManage filmManage = new FilmManage();
        Film film = new Film();


        private void textBoxFilm_TextChanged(object sender, EventArgs e)
        {

        }
       
        private void buttonSaveFilm_Click(object sender, EventArgs e)
        {
            film.Name=textBoxFilm.Text;
            film.Film_Genre = comboBoxFilmGenre.Text;
            film.Description = textBoxDescription.Text;
            //film.Poster = pictureBoxPoster.ImageLocation+pictureBoxPoster.Image;
            film.Saloon_ID = (int)comboBoxSaloon.SelectedValue;          
            film.Status = true;
            film.First_Presentation = dateTimePickerFirstPresentation.Value;
            film.Last_Presentation = dateTimePickerLastPresentation.Value;
            filmManage.insert(film);
            MessageBox.Show("Bir Kayıt Oluşturuldu");
            Clean();
        }

        #region filmseans
        //SessionManage sessionManage = new SessionManage();
        //sessionManage.insert(session);
        //session.Film_ID = Convert.ToInt32(filmManage.Lists().FirstOrDefault(k => k.Name == textBoxFilm.Text));
        //session.Session_Beginning = TimeSpan.Parse(maskedTextBoxSessionStart.Text);
        //session.Session_Finishing =u;
        //FilmDaySessionManage filmDaySessionManage = new FilmDaySessionManage();
        //Film_Day_Session film_Day_Session = new Film_Day_Session();
        //filmDaySessionManage.insert(film_Day_Session);
        //film_Day_Session.Session_ID = 1;
        //film_Day_Session.Film_ID= Convert.ToInt32(filmManage.Lists().FirstOrDefault(k => k.Name == textBoxFilm.Text));
        //film_Day_Session.Session_Date = dateTimePickerFirstPresentation.Value; 
        #endregion


        #region Private Initialize Methods
        /// <summary>
        /// 
        /// </summary>
        private void GenerateDataGrid()
        {
            dataGridViewSession.ColumnCount = 2;
            dataGridViewSession.ColumnHeadersVisible = true;

            dataGridViewSession.Columns[0].Name = "Start";
            dataGridViewSession.Columns[1].Name = "Finish";
        }
        /// <summary>
        /// 
        /// </summary>
        private void FillGenreList()
        {
            List<string> Genre = new List<string>();
            Genre.Add("Comedy");
            Genre.Add("Drama");
            Genre.Add("Sci-Fi");
            comboBoxFilmGenre.DataSource = Genre;
        }
        /// <summary>
        /// 
        /// </summary>
        private void FillSaloonList()
        {
            comboBoxSaloon.DataSource = salonManage.Lists();
            comboBoxSaloon.DisplayMember = "Name";
            comboBoxSaloon.ValueMember = "ID";
        }
        #endregion

        private void comboBoxSaloon_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            int filmid = (int)comboBoxFilmList.SelectedValue;
            film.ID = filmid;
            film.Name = comboBoxFilmList.Text;
            film.Film_Genre = comboBoxFilmGenre.Text;
            film.Description = textBoxDescription.Text;
            //film.Poster = pictureBoxPoster.ImageLocation+pictureBoxPoster.Image;
            film.Saloon_ID = (int)comboBoxSaloon.SelectedValue;
            film.Status = true;
            film.First_Presentation = dateTimePickerFirstPresentation.Value;
            film.Last_Presentation = dateTimePickerLastPresentation.Value;
            filmManage.update(film);
            Clean();
        }

        SqlConnection baglanti = new SqlConnection("Server =(local); database=CinemaDB;trusted_connection=true;");

        public SqlConnection Baglanti { get => baglanti; set => baglanti = value; }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (baglanti.State == ConnectionState.Closed)
                {
                    baglanti.Open();
                }
                int ara = Convert.ToInt32(comboBoxFilmList.SelectedValue);
                SqlCommand komut = new SqlCommand("Delete from Film Where ID='" + ara + "' ", baglanti);
                int say = komut.ExecuteNonQuery();
                if (say > 0)
                {
                    MessageBox.Show("Bir Kayıt Silindi");
                    Clean();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                baglanti.Close();
            }
        }
        private void Clean()
        {
            textBoxFilm.Clear();
            textBoxDescription.Clear();
            
        }

        private void dataGridViewSession_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
