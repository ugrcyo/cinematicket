﻿namespace CinemaApp
{
    partial class FrmSaloon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSaloonName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxStatusActive = new System.Windows.Forms.CheckBox();
            this.textBoxRowCount = new System.Windows.Forms.TextBox();
            this.textBoxSeatCount = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRowCountUD = new System.Windows.Forms.TextBox();
            this.textBoxSeatCountUD = new System.Windows.Forms.TextBox();
            this.comboBoxSaloonList = new System.Windows.Forms.ComboBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewSalonList = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSalonList)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxSaloonName
            // 
            this.textBoxSaloonName.Location = new System.Drawing.Point(174, 69);
            this.textBoxSaloonName.Multiline = true;
            this.textBoxSaloonName.Name = "textBoxSaloonName";
            this.textBoxSaloonName.Size = new System.Drawing.Size(275, 23);
            this.textBoxSaloonName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(10, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 34);
            this.label1.TabIndex = 1;
            this.label1.Text = "Salon Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(12, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 33);
            this.label2.TabIndex = 1;
            this.label2.Text = "Row Count";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(9, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 33);
            this.label3.TabIndex = 1;
            this.label3.Text = "Seat Count";
            // 
            // checkBoxStatusActive
            // 
            this.checkBoxStatusActive.AutoSize = true;
            this.checkBoxStatusActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.checkBoxStatusActive.Location = new System.Drawing.Point(179, 194);
            this.checkBoxStatusActive.Name = "checkBoxStatusActive";
            this.checkBoxStatusActive.Size = new System.Drawing.Size(204, 37);
            this.checkBoxStatusActive.TabIndex = 2;
            this.checkBoxStatusActive.Text = "Status Active";
            this.checkBoxStatusActive.UseVisualStyleBackColor = true;
            // 
            // textBoxRowCount
            // 
            this.textBoxRowCount.Location = new System.Drawing.Point(174, 110);
            this.textBoxRowCount.Multiline = true;
            this.textBoxRowCount.Name = "textBoxRowCount";
            this.textBoxRowCount.Size = new System.Drawing.Size(275, 26);
            this.textBoxRowCount.TabIndex = 0;
            // 
            // textBoxSeatCount
            // 
            this.textBoxSeatCount.Location = new System.Drawing.Point(174, 148);
            this.textBoxSeatCount.Multiline = true;
            this.textBoxSeatCount.Name = "textBoxSeatCount";
            this.textBoxSeatCount.Size = new System.Drawing.Size(275, 27);
            this.textBoxSeatCount.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.Location = new System.Drawing.Point(179, 237);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 45);
            this.button1.TabIndex = 3;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(295, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(321, 42);
            this.label4.TabIndex = 4;
            this.label4.Text = "Saloon IUD Page";
            // 
            // textBoxRowCountUD
            // 
            this.textBoxRowCountUD.Location = new System.Drawing.Point(478, 110);
            this.textBoxRowCountUD.Multiline = true;
            this.textBoxRowCountUD.Name = "textBoxRowCountUD";
            this.textBoxRowCountUD.Size = new System.Drawing.Size(275, 26);
            this.textBoxRowCountUD.TabIndex = 0;
            // 
            // textBoxSeatCountUD
            // 
            this.textBoxSeatCountUD.Location = new System.Drawing.Point(478, 148);
            this.textBoxSeatCountUD.Multiline = true;
            this.textBoxSeatCountUD.Name = "textBoxSeatCountUD";
            this.textBoxSeatCountUD.Size = new System.Drawing.Size(275, 27);
            this.textBoxSeatCountUD.TabIndex = 0;
            // 
            // comboBoxSaloonList
            // 
            this.comboBoxSaloonList.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboBoxSaloonList.FormattingEnabled = true;
            this.comboBoxSaloonList.Location = new System.Drawing.Point(478, 58);
            this.comboBoxSaloonList.Name = "comboBoxSaloonList";
            this.comboBoxSaloonList.Size = new System.Drawing.Size(275, 45);
            this.comboBoxSaloonList.TabIndex = 5;
            this.comboBoxSaloonList.SelectedIndexChanged += new System.EventHandler(this.comboBoxSaloonList_SelectedIndexChanged);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonUpdate.Location = new System.Drawing.Point(487, 237);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(129, 45);
            this.buttonUpdate.TabIndex = 6;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button2.Location = new System.Drawing.Point(624, 237);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 45);
            this.button2.TabIndex = 7;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewSalonList);
            this.groupBox1.Location = new System.Drawing.Point(-3, 343);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(839, 184);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Salon List";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dataGridViewSalonList
            // 
            this.dataGridViewSalonList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSalonList.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewSalonList.Name = "dataGridViewSalonList";
            this.dataGridViewSalonList.Size = new System.Drawing.Size(833, 159);
            this.dataGridViewSalonList.TabIndex = 0;
            this.dataGridViewSalonList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSalonList_CellContentClick);
            // 
            // FrmSaloon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 528);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.comboBoxSaloonList);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBoxStatusActive);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSeatCountUD);
            this.Controls.Add(this.textBoxSeatCount);
            this.Controls.Add(this.textBoxRowCountUD);
            this.Controls.Add(this.textBoxRowCount);
            this.Controls.Add(this.textBoxSaloonName);
            this.Name = "FrmSaloon";
            this.Text = "FrmSaloon";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSaloon_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSalonList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSaloonName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxStatusActive;
        private System.Windows.Forms.TextBox textBoxRowCount;
        private System.Windows.Forms.TextBox textBoxSeatCount;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRowCountUD;
        private System.Windows.Forms.TextBox textBoxSeatCountUD;
        private System.Windows.Forms.ComboBox comboBoxSaloonList;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewSalonList;
    }
}