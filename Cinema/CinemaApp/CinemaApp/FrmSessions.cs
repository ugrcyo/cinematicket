﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBaseLayer;
using ServiceLayer.Common.DTOS;
using ServiceLayer.Manage;

namespace CinemaApp
{
    public partial class FrmSessions : Form
    {
        public FrmSessions()
        {
            InitializeComponent();
        }
        FilmManage flm = new FilmManage();
        SessionManage smng = new SessionManage();
        Session session = new Session();
       
        CinemaDBEntities db = new CinemaDBEntities();
        SqlConnection baglanti = new SqlConnection("Server =(local); database=CinemaDB;trusted_connection=true;");

        public SqlConnection Baglanti { get => baglanti; set => baglanti = value; }

        private void FrmSessions_Load(object sender, EventArgs e)
        {
            comboBoxFilmLists.DataSource = flm.Lists();
            comboBoxFilmLists.DisplayMember = "Name";
            comboBoxFilmLists.ValueMember = "ID";

            dataGridView1.DataSource =smng.Lists();

        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            session.Film_ID = (int)comboBoxFilmLists.SelectedValue;
            session.Session_Beginning = TimeSpan.Parse(maskedTextBoxBegin.Text);
            TimeSpan a = TimeSpan.Parse(maskedTextBoxFilmDuration.Text);
            session.Film_Duration = a;           
            session.Session_Finishing = a.Add(session.Session_Beginning);           
            smng.insert(session);
            
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            
         
            session.Film_Duration =TimeSpan.Parse(maskedTextBoxFilmDuration.Text);
            session.Session_Beginning = TimeSpan.Parse(maskedTextBoxBegin.Text);
            TimeSpan a = TimeSpan.Parse(maskedTextBoxFilmDuration.Text);
            session.Film_Duration = a;
            session.Session_Finishing = a.Add(session.Session_Beginning);

            string updateResult = smng.update(session);

            MessageBox.Show(updateResult);
        }
        
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            maskedTextBoxFilmDuration.Text = dataGridView1.CurrentRow.Cells["Film_Duration"].Value.ToString();
            maskedTextBoxBegin.Text = dataGridView1.CurrentRow.Cells["Session_Beginning"].Value.ToString();
            session.ID = (int)dataGridView1.CurrentRow.Cells["ID"].Value;
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (baglanti.State == ConnectionState.Closed)
                {
                    baglanti.Open();
                }
                int ara = Convert.ToInt32((int)dataGridView1.CurrentRow.Cells["ID"].Value);
                SqlCommand komut = new SqlCommand("Delete from Session Where ID='" + ara + "' ", baglanti);
                int say = komut.ExecuteNonQuery();
                if (say > 0)
                {
                    MessageBox.Show("Bir Kayıt Silindi");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                baglanti.Close();
            }
        }
    }
}
